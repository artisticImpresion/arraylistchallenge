package dev.training.collections;
/**
 * 
 */

import java.util.ArrayList;

import com.sun.jmx.snmp.tasks.ThreadService;

/**
 * @author artisticImpresion 30/12/2018
 *
 */
public class MobilePhone {
	
	//private ArrayList<Contact> contactList = new ArrayList<Contact>();
	private ArrayList<Contact> contactList;
	
	public MobilePhone() {
		this.contactList = new ArrayList<Contact>();
	}
	
//	public void addContact(String firstName, String lastName, String phoneNumber) {
//		//TODO true when contact doesn't exists
//		if (true) {
//			Contact contactToAdd = Contact.createNewContact(firstName, lastName, phoneNumber);
//			contactList.add(contactToAdd);
//		}
//	}
	
	public boolean addContact(Contact contact) {
		if(findContact(contact.getLastName()) >= 0) {
			System.out.println("Contact already exists!");
			return false;
		}
		contactList.add(contact);
		return true;
	}

	public void displayContacts() {
		if(contactList.isEmpty()) {
			System.out.println("No contacts yet!");
		} else {
			System.out.println("Contact list:");
//			for (Contact contact : contactList) {
			for(int i=0; i<contactList.size(); i++) {
				System.out.println((i+1) + ". " + 
								  this.contactList.get(i));
			}
		}
		
	}
	
	public boolean updateContact(Contact oldContact, Contact newContact) {
		int foundPosition = findContact(oldContact);
		if(foundPosition < 0) {
			System.out.println("Unable to find '" + oldContact.getLastName() + "' contact!");
			return false;
		} else if (findContact(newContact.getLastName()) != -1) {
			System.out.println("Contact with last name '" + newContact.getLastName() + "' already exists!");
			return false;
		}
		
		this.contactList.set(foundPosition, newContact);
		System.out.println(oldContact.getLastName() + ", was replaced with " + newContact.getLastName());
		return true;
	}
	
	public boolean removeContact(Contact contact) {
		int foundPosition = findContact(contact);
		if(foundPosition < 0) {
			System.out.println("Contact doesn't exist! Unable to remove non-existing contact!");
			return false;
		}
		this.contactList.remove(foundPosition);
		System.out.println("Contact with last name '" + contact.getLastName() + "' removed successfully!");	
		return true;
	}
	
	private int findContact(Contact contact) {
		return this.contactList.indexOf(contact);
	}
	
	private int findContact(String contactName) {
		for(int i=0; i<this.contactList.size(); i++) {
			Contact contact = this.contactList.get(i);
			if(contact.getLastName().equals(contactName)) {
				return i;
			}
		}
		return -1;
	}
	
	public String queryContact(Contact contact) {
		if(findContact(contact) >= 0) {
			return contact.getLastName();
		}
		return null;
	}
	
	public Contact queryContact(String lastName) {
		int contactPosition = findContact(lastName);
		if(contactPosition >= 0) {
			return this.contactList.get(contactPosition);
		}
		return null;
	}
	
}
