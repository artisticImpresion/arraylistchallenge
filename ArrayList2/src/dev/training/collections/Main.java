package dev.training.collections;
/**
 * 
 */

import java.util.Scanner;

/**
 * @author artisticImpresion 30/12/2018
 *
 */
public class Main {
	
	private static Scanner scanner = new Scanner(System.in);
	private static MobilePhone phone = new MobilePhone();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean quit = false;
		printInstructions();
		while(!quit) {
			System.out.println("Enter your choice: ");
			int choice = scanner.nextInt();
			scanner.nextLine();
			switch (choice) {
			case 0:
				printInstructions();
				break;
			case 1:
				printContacts();
				break;
			case 2:
				addNewContact();
				break;
			case 3:
				modifyContact();
				break;
			case 4:
				deleteContact();
				break;
			case 5:
				searchContact();
				break;
			case 6:
				quit = true;
				System.out.println("Bye!");
				break;
			}
		}
	}

	public static void printInstructions() {
        System.out.println("\nPress ");
        System.out.println("\t 0 - To print choice options.");
        System.out.println("\t 1 - To print the Contact list.");
        System.out.println("\t 2 - To add a new Contact to the Contact list.");
        System.out.println("\t 3 - To modify an existing Contact.");
        System.out.println("\t 4 - To remove a Contact from the Contact list.");
        System.out.println("\t 5 - To search for a Contact in the Contact list.");
        System.out.println("\t 6 - To quit the application.");
    }
	
	private static void printContacts() {
		phone.displayContacts();
	}
	
	private static void addNewContact() {
		System.out.println("To add a new contact, please type in following information:");
		System.out.print("First name: ");
		String firstName = scanner.nextLine();
		System.out.print("Last name: ");
		String lastName = scanner.nextLine();
		System.out.print("Phone number (only number format): ");
		String phoneNumber = scanner.nextLine();
		Contact newContact = Contact.createNewContact(firstName, lastName, phoneNumber); 
		if(phone.addContact(newContact)) {
			System.out.println("New contact added successfully: " + newContact);
		} else {
			System.out.println("Unable to add following contact: " + newContact);
		}
	}
	
	public static void modifyContact() {
		System.out.print("Enter existing contact last name: ");
		String lastName = scanner.nextLine();
		Contact existingContactRecord = phone.queryContact(lastName);
		if(existingContactRecord == null) {
			System.out.println("Unable to find '" + lastName + "' in the Contact list!");
			System.out.println("Action aborted!");
			return;
		}
		
		System.out.println("Enter new contact last name: ");
		String newLastName = scanner.nextLine();
		System.out.println("Enter new contact first name: ");
		String newFirstName = scanner.nextLine();
		System.out.println("Enter new phone number: ");
		String newPhoneNumber = scanner.nextLine();
		
		Contact newContactRecord = Contact.createNewContact(newFirstName, newLastName, newPhoneNumber);
		if(phone.updateContact(existingContactRecord, newContactRecord)) {
			System.out.println("'" + existingContactRecord + "' modified to: '" + 
								newContactRecord + "'");
		} else {
			System.out.println("Update was not successful!");
		}
	}
	
	public static void deleteContact() {
		System.out.print("Enter existing contact last name: ");
		String lastName = scanner.nextLine();
		Contact existingContactRecord = phone.queryContact(lastName);
		if(existingContactRecord == null) {
			System.out.println("Unable to find '" + lastName + "' in the Contact list!");
			System.out.println("Action aborted!");
			return;
		}
		
		if(phone.removeContact(existingContactRecord)) {
			System.out.println("Contact record successfully deleted!");
		} else {
			System.out.println("Error while deleting contact record!");
		}
	}
	
	public static void searchContact() {
		System.out.print("Please type in a last name you want to search in Contacts:");
		String searchName = scanner.nextLine();
		Contact existingContactRecord = phone.queryContact(searchName);
		if(existingContactRecord == null) {
			System.out.println("Unable to find '" + searchName + "' in the Contact list!");
			return;
		}
		System.out.println(existingContactRecord);
		System.out.println("You were looked for last name:'" + searchName);
		System.out.println("Following contact record was found: ");
		System.out.println(existingContactRecord.getFirstName() + " " + existingContactRecord.getLastName() + " has phone number: " + existingContactRecord.getPhoneNumber());
	}
}
