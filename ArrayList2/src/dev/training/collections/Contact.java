package dev.training.collections;
/**
 * 
 */

/**
 * @author artisticImpresion 30/12/2018
 *
 */
public class Contact {
	private String firstName;
	private String lastName;
	private int phoneNumber;
	
	public Contact(String firstName, String lastName, String phoneNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = Integer.parseInt(phoneNumber);
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String toString() {
		String result = getFirstName() + " " + getLastName() + " -> " + getPhoneNumber();
		return result;
	}
	
	public static Contact createNewContact(String firstName, String lastName, String phoneNumber) {
		return new Contact(firstName, lastName, phoneNumber);
	}
	
}
